from lxml import etree
from pathlib import Path
import zipfile, os, re, shutil, copy

str='<w:tr w:rsidR="007D1A07" w14:paraId="0A755D92" w14:textId="77777777" w:rsidTr="007D1A07"><w:tc><w:tcPr><w:tcW w:w="2337" w:type="dxa"/></w:tcPr><w:p w14:paraId="05AC5B49" w14:textId="74B9C485" w:rsidR="007D1A07" w:rsidRDefault="007D1A07" w:rsidP="007D1A07"><w:r><w:t>Test A</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2337" w:type="dxa"/></w:tcPr><w:p w14:paraId="4036C2E1" w14:textId="62D92B6C" w:rsidR="007D1A07" w:rsidRDefault="007D1A07" w:rsidP="007D1A07"><w:r><w:t>2 Test A</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2338" w:type="dxa"/></w:tcPr><w:p w14:paraId="0FDF16E5" w14:textId="418EC28E" w:rsidR="007D1A07" w:rsidRDefault="007D1A07" w:rsidP="007D1A07"><w:r><w:t>3 A</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2338" w:type="dxa"/></w:tcPr><w:p w14:paraId="7C7E0AE8" w14:textId="4BF6B02A" w:rsidR="007D1A07" w:rsidRDefault="007D1A07" w:rsidP="007D1A07"><w:r><w:t>4 A</w:t></w:r></w:p></w:tc></w:tr>'
mods=[['Test 1 A','Test 2 A','Test 3 A','Test 4 A'],
	['Test 1 B','Test 2 B','Test 3 B','Test 4 B'],
	['Test 1 C','Test 2 C','Test 3 C','Test 4 C']]

with open('sample/word/document.xml','r') as f:
	isColumn=False
	content=f.read().encode('utf-8')
	tree=etree.fromstring(content)
	root=tree.getchildren()
	for body in root:
		for tbl in body:
			isIssue=False
			
			for tr in tbl:
				nextrow=tr.getnext()
				el=copy.deepcopy(tr)
				i=0
				if isIssue:
					tbl.remove(tr)
					continue
				for attr in tr.attrib:
					# print(attr)
					if 'paraId' in attr:
						if '0A755D92' in tr.attrib[attr]:
							for tc in tr:
								for p in tc:
									if p.tag[(p.tag.find('}')+1):] == 'p':
										for r in p:
											for t in r:
												isIssue=True
												t.text=mods[0][i]
												i+=1
										# if '74B9C485' in p.attrib[attr2]:
											# isColumn=True
											# for r in p:
												# for t in r:
													# print(t.text)
													# t.text='Test 1'
				if isIssue:
					print('add row')
					tr.addnext(el)
					for i in range(len(mods)-1, 0, -1):
						print(i)
						j=0
						column=el.find('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}tc')
						while column != None:
							print(column)
							column.find('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}p') \
							.find('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}r') \
							.find('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}t') \
							.text=mods[i][j]
							print(mods[i][j])
							j+=1
							tr.addnext(el)
							column=column.getnext()
						el=copy.deepcopy(el)
					
	path = Path('test.docx')
	if path.exists():
		os.remove('test.docx')
	os.mkdir('test')
	os.system('cp -R sample/* test/')
	new_zp=zipfile.ZipFile('test.zip', 'w')
	f = open('test/word/document.xml','w')
	f.write('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>')
	f.write(etree.tostring(tree).decode('utf-8'))
	f.close()
	
	for root, dirs, files in os.walk('test'):
		for f in files:
			new_root=re.sub('test','',root)
			if len(new_root) > 0:
				new_root+='\\'
			new_zp.write(root+'\\'+f, new_root[1:]+f)
	new_zp.close()
	os.rename('test.zip','test.docx')
	shutil.rmtree('test')