import zipfile
from lxml import etree
from shutil import copyfile
import os, re, shutil

path='sample.docx'
mod='sample_mod.zip'
new_mod='new_sample'

tree=None

mods=[
        [ 'Section 1', '1 A', '2 AA', '3 AA', '4 AA'],
        [ 'Section 2', 'dsp_image', '5/5/19', '454545', 'n/a']
    ]

class Item():
        def __init__(self):
            self.phrases=[]
            self.matches=0

def checkForMatch(node, item, n):
    if item.matches >= len(mods[n]):
        return
		
    if node.text == mods[n][item.matches]:
        item.phrases.append(mods[n][item.matches])
        item.matches+=1
    elif item.matches >= 2:
        if node.text != mods[n][item.matches]:
            print('updating {} {}'.format(item.phrases, mods[n][item.matches]))
            node.text = mods[n][item.matches]
            item.matches+=1

    return item.matches
	
def getChildren(node):
    return node.getchildren()

def iterChildren(node, item, i):
    for n in node:
        if n.tag[(n.tag.find('}')+1):] == 't':
            checkForMatch(n, item, i)
        n = getChildren(n)
        iterChildren(n,item,i)

def replaceString(fp,orig,mod):
    f=open(fp, 'r')
    s=f.read()
    offs=s.find(orig)+len(orig)
    s2=s[:offs-len(orig)]
    s1=s[offs:]
    s=s2+mod+s1
    with open(fp, 'w') as f:
        f.write(s)

def writeContents(data):
	zp=zip.extractall(new_mod)
	new_zp=zipfile.ZipFile(new_mod+'.zip', 'w')
	
	f = open(new_mod+'/word/document.xml','w')
	f.write('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>')
	f.write(data.decode('utf-8'))
	f.close()
	
	for root, dirs, files in os.walk(new_mod):
		for f in files:
			new_root=re.sub(new_mod,'',root)
			if len(new_root) > 0:
				new_root+='\\'
			new_zp.write(root+'\\'+f, new_root[1:]+f)
	
	new_zp.close()
	shutil.rmtree(new_mod)
	os.rename(new_mod+'.zip',new_mod+'.docx')

copyfile(path, mod)

with zipfile.ZipFile(mod) as zip:
    content=zip.read('word/document.xml')
    tree=etree.fromstring(content)
    node=getChildren(tree)
    for n in range(len(mods)):
        item=Item()
        iterChildren(node,item,n)
    content=etree.tostring(tree)
    writeContents(content)
    #replaceString('sample_output.xml','Test2','test2.5')